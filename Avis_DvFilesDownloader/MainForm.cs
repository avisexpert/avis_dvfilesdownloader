﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocsVision.Platform.ObjectManager;

namespace Avis_DvFilesDownloader
{
    public partial class MainForm : Form
    {

        static readonly int LOG_LINES_MAX_COUNT = 10;
        static readonly Guid DOCUMENT_FILES_SECTION_ID = new Guid("A6FA8BAF-2EA4-4071-AA3E-5C4E71646A90");
        static readonly Guid VERSIONED_FILE_CARD_VERSIONS_SECTION_ID = new Guid("F831372E-8A76-4ABC-AF15-D86DC5FFBE12");

        private string logfilename;
        private System.IO.StreamWriter logfile;
        public MainForm()
        {
            InitializeComponent();



            this.logfilename = System.IO.Path.Combine(this.logFolderTextBox.Text, "Log_" + DateTime.Now.ToString("yyMMdd_hhmmss") + ".log");
            this.initlogfile();

            this.Logmessage("Started");


        }


        /// <summary>
        /// Initializes log file
        /// </summary>
        private void initlogfile()
        {
            this.logfile = new System.IO.StreamWriter(this.logfilename);
            
        }

        /// <summary>
        /// Writes message to both log on form and logfile
        /// </summary>
        /// <param name="message">message to be written</param>
        private void Logmessage(string message)
        {
            string fulllogmessage = string.Format("{0} {1}", DateTime.Now.ToString(), message);
            string currenttext = this.logTextBox.Text;
            string[] lines = currenttext.Split(new[] { "\r\n" }, StringSplitOptions.None);
            int linescount = lines.Length;
            //Truncate first line, log will rollup
            if (linescount > LOG_LINES_MAX_COUNT)
            {
                lines = lines.Skip(1).ToArray();
                currenttext = string.Join("\r\n", lines);
            }



            this.logTextBox.Text = currenttext + fulllogmessage + "\r\n";
            this.logfile.WriteLine(fulllogmessage);

        }

        private void MainForm_Deactivate(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Returns string rpresenrting path for exporting version
        /// </summary>
        /// <param name="documentfolder">path for document folder: Root_Folder\DocumentId</param>
        /// <param name="filetype">0 - Main, 1 - Additional</param>
        /// <param name="filename">DV file name</param>
        /// <returns></returns>
        private string GetExportFilePath(string documentfolder, string filetype, string filename)
        {
            filename = filename.Replace(";", "_");
            string filepath = System.IO.Path.Combine(documentfolder, filetype == "0" ? "Main" : "Additional", filename);
            if (System.IO.File.Exists(filepath))
            {
                string shortfilename = System.IO.Path.GetFileNameWithoutExtension(filepath);
                string extension = System.IO.Path.GetExtension(filepath);
                filepath = System.IO.Path.Combine(documentfolder, filetype == "0" ? "Main" : "Additional", shortfilename + "_" +  Guid.NewGuid() + extension);
                Logmessage("File already exists!");
            }
            

            return filepath;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            //string fileids = this.docidsTextBox.Text;

           
            string rootfolder = this.rootFolderTextBox.Text;
            string logfolder = this.logFolderTextBox.Text;
            if (System.IO.File.Exists(this.docIdsFile.Text))
            {
                string fileids = System.IO.File.ReadAllText(this.docIdsFile.Text);
                if (!string.IsNullOrEmpty(rootfolder))
                {
                    if (!string.IsNullOrEmpty(logfolder))
                    {
                        if (!string.IsNullOrEmpty(fileids))
                        {


                            string connectionstring = this.connectionAddress.Text;
                            ///Creating Docsvision session
                            if (!string.IsNullOrEmpty(connectionstring))
                            {
                                SessionManager sm = SessionManager.CreateInstance();
                                try
                                {
                                    sm.Connect(connectionstring);
                                    UserSession session = sm.CreateSession();
                                    Logmessage("User session created");

                                    CardManager cm = session.CardManager;
                                    FileManager fileManager = session.FileManager;

                                    string[] cardsstrarray = fileids.Split(new[] { "\r\n" }, StringSplitOptions.None);

                                    this.pb.Maximum = cardsstrarray.Length;

                                    //Reading list of card identifies
                                    foreach (string cardidstr in cardsstrarray)
                                    {
                                        try
                                        {

                                            Guid card_id = new Guid(cardidstr);

                                            Logmessage(string.Format("DocumentId = {0}", cardidstr));
                                            // Creating folder for document
                                            string documentsfolderpath = System.IO.Path.Combine(rootfolder, cardidstr);
                                            if (!System.IO.Directory.Exists(documentsfolderpath))
                                                System.IO.Directory.CreateDirectory(documentsfolderpath);

                                            string mainfilesfolder = System.IO.Path.Combine(documentsfolderpath, "Main");
                                            string additionalfilesfolder = System.IO.Path.Combine(documentsfolderpath, "Additional");
                                            if (!System.IO.Directory.Exists(mainfilesfolder))
                                                System.IO.Directory.CreateDirectory(mainfilesfolder);
                                            if (!System.IO.Directory.Exists(additionalfilesfolder))
                                                System.IO.Directory.CreateDirectory(additionalfilesfolder);

                                            //    Logmessage("Processing card withid = " + card_id.ToString());
                                            //Getting Document cards
                                            CardData cd = cm.GetCardData(card_id);
                                            RowDataCollection rdc = cd.Sections[DOCUMENT_FILES_SECTION_ID].Rows;
                                            foreach (RowData rd in rdc)
                                            {
                                                try
                                                {
                                                    //Getting Versioned file cards
                                                    Guid fileId = new Guid(rd["FileId"].ToString());
                                                    string filetype = rd["FileType"].ToString();
                                                    //type = 0 - main
                                                    //type = 1 - additional
                                                    string filename = rd["FileName"].ToString();
                                                    int currentversion = Convert.ToInt32(rd["FileCurrentVersion"].ToString());
                                                    Logmessage(string.Format("VersionedFileId = {0}, type = {1}, Filename = {2}, version = {3}", fileId.ToString(), filetype, filename, currentversion));
                                                    CardData versionedfilecardcd = cm.GetCardData(fileId);
                                                    // Getting current version row
                                                    RowDataCollection rdcversions = versionedfilecardcd.Sections[VERSIONED_FILE_CARD_VERSIONS_SECTION_ID].GetAllRows();
                                                    RowData versionrow = rdcversions.Find("VersionNumber", currentversion);

                                                    if (versionrow != null)
                                                    {
                                                        Guid FileID = new Guid(versionrow["FileID"].ToString());
                                                        string exportFileName = GetExportFilePath(documentsfolderpath, filetype, filename);
                                                        FileData fileForExport = fileManager.GetFile(FileID);
                                                        fileForExport.Download(exportFileName);

                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    Logmessage("ERROR during processing file with row_id =  " + rd.Id.ToString() + " " + ex.Message + " " + ex.StackTrace);
                                                }
                                            }

                                            this.pb.Value++;
                                            this.logfile.Flush();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logmessage("ERROR during processing document with id =  " + cardidstr + " " + ex.Message + " " + ex.StackTrace);
                                        }
                                    }

                                    session.Close();


                                    Logmessage("Finished");
                                }
                                catch (Exception ex)
                                {
                                    Logmessage("ERROR during data prepare " + ex.Message + " " + ex.StackTrace);
                                }
                            }
                            else
                                Logmessage("Connection address not set!");
                        }
                        else
                            Logmessage("Document ids not set!");
                    }
                    else
                        Logmessage("Log folder not set!");
                }
                else
                    Logmessage("Root folder not set!");
            }
            else
            Logmessage("file with document ids doesn't exist!");

        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.logfile.Close();
        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }
    }
}
