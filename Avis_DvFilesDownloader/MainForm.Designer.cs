﻿namespace Avis_DvFilesDownloader
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectionAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.logFolderTextBox = new System.Windows.Forms.TextBox();
            this.docidsTextBox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.rootFolderTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pb = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.docIdsFile = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connectionAddress
            // 
            this.connectionAddress.Location = new System.Drawing.Point(162, 12);
            this.connectionAddress.Name = "connectionAddress";
            this.connectionAddress.Size = new System.Drawing.Size(616, 20);
            this.connectionAddress.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Connection address";
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(25, 345);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.Size = new System.Drawing.Size(763, 150);
            this.logTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Log folder";
            // 
            // logFolderTextBox
            // 
            this.logFolderTextBox.Location = new System.Drawing.Point(162, 38);
            this.logFolderTextBox.Name = "logFolderTextBox";
            this.logFolderTextBox.Size = new System.Drawing.Size(616, 20);
            this.logFolderTextBox.TabIndex = 5;
            // 
            // docidsTextBox
            // 
            this.docidsTextBox.Location = new System.Drawing.Point(162, 117);
            this.docidsTextBox.Multiline = true;
            this.docidsTextBox.Name = "docidsTextBox";
            this.docidsTextBox.ReadOnly = true;
            this.docidsTextBox.Size = new System.Drawing.Size(616, 164);
            this.docidsTextBox.TabIndex = 6;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(162, 287);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(616, 23);
            this.startButton.TabIndex = 7;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // rootFolderTextBox
            // 
            this.rootFolderTextBox.Location = new System.Drawing.Point(162, 65);
            this.rootFolderTextBox.Name = "rootFolderTextBox";
            this.rootFolderTextBox.Size = new System.Drawing.Size(616, 20);
            this.rootFolderTextBox.TabIndex = 8;
            this.rootFolderTextBox.Text = "D:\\DVContractsExport";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Root export folder";
            // 
            // pb
            // 
            this.pb.Location = new System.Drawing.Point(162, 316);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(616, 23);
            this.pb.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Progress";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Documents ID list";
            this.label4.Click += new System.EventHandler(this.Label4_Click);
            // 
            // docIdsFile
            // 
            this.docIdsFile.Location = new System.Drawing.Point(162, 91);
            this.docIdsFile.Name = "docIdsFile";
            this.docIdsFile.Size = new System.Drawing.Size(616, 20);
            this.docIdsFile.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "File with id\'s of loading files";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 507);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.docIdsFile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rootFolderTextBox);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.docidsTextBox);
            this.Controls.Add(this.logFolderTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.connectionAddress);
            this.Name = "MainForm";
            this.Text = "Avis Expert Docsvision file export";
            this.Deactivate += new System.EventHandler(this.MainForm_Deactivate);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox connectionAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox logFolderTextBox;
        private System.Windows.Forms.TextBox docidsTextBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TextBox rootFolderTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar pb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox docIdsFile;
        private System.Windows.Forms.Label label6;
    }
}

